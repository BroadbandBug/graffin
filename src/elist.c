/**************************************************************************************************/
/*
 * File: elist.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following contains functions for the manipulation of an edge list
 *
 */
/**************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "global.h"
#include "edge.h"
#include "elist.h"

/**************************************************************************************************/

/* initialize elist to empty elist */
void 
elist_init(EdgeList *elist)
{
    if( elist == NULL ) return;
    
    elist->head = NULL;
    elist->tail = NULL;
    elist->size = 0;
}

/**************************************************************************************************/

/* destroy the elist */
void 
elist_destroy(EdgeList *elist)
{
    if( elist == NULL ) return;
    
    /* while the list of list elements is not empty, remove the element at the head of the list */
    while( elist->size > 0 ) {
        elist_remove(elist, elist->head);
    }
}

/**************************************************************************************************/

/* inserts new edge (Vertex *, weight) into elist */
bool 
elist_insert_edge(EdgeList *elist, Vertex *adjVirtex, int weight)
{
    if( elist == NULL ) return FALSE;

    return elist_insert_next(elist, elist->tail, adjVirtex, weight);
}

/**************************************************************************************************/

/* removes the specified edge from the vlist */
bool 
elist_remove(EdgeList *elist, Edge *edge)
{
    if( elist == NULL ) return FALSE;

    Edge *prevEdge;
    Edge *nextEdge;
    Edge *currentEdge;
    
    for (currentEdge = elist->head; currentEdge !=NULL; currentEdge = currentEdge->next) {
        
        if (currentEdge == edge) {
            
            //Update the pointers
            prevEdge = currentEdge->prev;
            nextEdge = currentEdge->next;
            
            prevEdge->next = nextEdge;
            nextEdge->prev = prevEdge;
            
            //Case: Edge being removed is the head
            //Update the elist head
            if (edge == elist->head) {
                elist->head = nextEdge;
            }
            //Case: Edge being removed is the tail
            //Update the elist tail
            else if(edge == elist->tail){
                elist->tail = prevEdge;
            }
            
            //Now remove the vertex
            free(edge);
            return TRUE;
        }
    }
    
    return FALSE;
}

/**************************************************************************************************/

/* inserts new edge into elist after the given elist element */
bool 
elist_insert_next(EdgeList *elist, Edge *edge, Vertex *adjVirtex, int weight)
{
    if( elist == NULL ) return FALSE;

    Edge *currentEdge;
    Edge *newEdge;
    
    //If the given element is Null, then insert at the head of the list
    if (edge == NULL) {
        edge = elist->head;
    }
    
    if((newEdge = (Edge *)malloc(sizeof(Edge)))== NULL){
        printf("\nError: Cannot allocate additional memory\n");
        return FALSE;
    }
    
    newEdge->weight = weight;
    newEdge->adjVertex = adjVirtex;
    
    //If the elist size is zero then the head and tail pointers must be updated.
    if (elist->size == 0) {
        elist->head = newEdge;
        elist->tail = newEdge;
        elist->size++;
        return TRUE;
    }
    for (currentEdge = elist->head; currentEdge !=NULL; currentEdge = currentEdge->next) {
        
        if (currentEdge == edge) {
            
            //Case: The edge pointer is the tail
            //Solution: Must update the elist->tail pointer
            if (edge == elist->tail) {
                edge->next = newEdge;
                newEdge->prev = edge;
                newEdge->next = NULL;
                elist->tail = newEdge;
            }
            //Case: the edge pointer is located somewhere in the middle of the list or at the head
            //Solution: must update update currentEdge->previous & currentEdge->next pointers
            else{
                newEdge->next = edge->next;
                edge->next->prev = newEdge;
                edge->next = newEdge;
                newEdge->prev = edge;
                
            }
            
            elist->size++;
            return TRUE;
        }
    }
    return FALSE;
}

/**************************************************************************************************/

/* inserts new edge into elist before the given elist element */
bool 
elist_insert_prev(EdgeList *elist, Edge *edge, Vertex *adjVirtex, int weight)
{
    if( elist == NULL ) return FALSE;
    
    Edge *currentEdge;
    Edge *newEdge;
    
    //If the given element is Null, then insert before the head of the list
    if (edge == NULL) {
        edge = elist->head;
    }
    
    for (currentEdge = elist->head; currentEdge !=NULL; currentEdge = currentEdge->next) {
        
        if (currentEdge == edge) {
            
            
            if((newEdge = (Edge *)malloc(sizeof(Edge)))== NULL){
                printf("\nError: Cannot allocate additional memory\n");
                return FALSE;
            }
            
            //If the elist size is zero then the head and tail pointers must be updated.
            if (elist->size == 0) {
                elist->head = newEdge;
                elist->tail = newEdge;
            }
            
            //Case: The edge pointer is the head
            //Solution: Must update the elist->head pointer
            if (edge == elist->head) {
                newEdge->next = edge;
                edge->prev = newEdge;
                newEdge->prev = NULL;
                elist->head = newEdge;
            }
            //Case: the edge pointer is located somewhere in the middle of the list or at the tail
            //Solution: must update update currentEdge->previous & currentEdge->next pointers
            else{
                newEdge->next = edge;
                newEdge->prev = edge->prev;
                edge->prev->next = newEdge;
                edge->prev = newEdge;
                
            }
            
            elist->size++;
            return TRUE;
        }
    }
    return FALSE;
}

/**************************************************************************************************/

