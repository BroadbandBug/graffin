/**************************************************************************************************/
/*
 * File: 
 * Author: Kevin Klug
 * Date Created: 6 December 2011
 *
 * Description: Code for the implementation of graffin, Assignment 5 for ECE275a
 *              For more information, please see the README
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "graph.h"
#include "graffin.h"
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

/**************************************************************************************************/
int 
main(int argc, char *argv[]) 
{
    //Check for silly users
    if (argc != 2) {
        printf("\nUsage: graffin graphFile\n");
        return FALSE;
    }
    
    //Declare a graph
    Graph my_graph;
    
    //Initalize a graph
    graph_init(&my_graph);
    
    //Populate the graph
    populate_graph(&my_graph, argv[1]);
    
    //Analyze the graph
    analyze_graph(&my_graph);
    
    //Burn the graph with fire
    graph_destroy(&my_graph);
    
    //That's all folks
    return 0;
}

/**************************************************************************************************/
