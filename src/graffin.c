/**************************************************************************************************/
/*
 * File: graffin.c
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following are functions used in the implementation of graffin
 *
 */
/**************************************************************************************************/

#include "graffin.h"
#include "vlist.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>	
//Yay functions!

//This function will read the inputfile and populate graph
bool populate_graph( Graph *graph, char *inputFileName){
    
    FILE *inputFile;
    Vertex *vertex;
    Vertex *adjVertex;
    //Assuming max line length is 1000 characters
    char buffLine[500];
    char buffString[10];
    int numVertices = 0;
    int numHubs = 0;
    int hubID = 0;
    bool edgeDetect = FALSE;
    
    int startVertexID = 0;
    int endVertexID = 0;
    int edgeWeight = 0;
    
    //integer value of hub for scanning
    int i = 0;

    
    inputFile = fopen(inputFileName, "r");
    
    
    //Check to make sure file is accessable
    if (inputFile == NULL){
        printf("Error: Cannot open file\n");
        exit(-1);
    }
    
    //******************************
    //SCANNING IN NUMBER OF VERTICES
    //******************************
    
    //Scan in one line of the file until the string "Vertices" is matched
    while (fgets(buffLine, sizeof(buffLine), inputFile) != NULL) {
        
        //Scan the buffLine for the first string
        sscanf(buffLine, "%s", buffString);
        
        if (strcmp(buffString, "Vertices:")==0) {
            //The decimal value on this line is the number of vertices
            sscanf(buffLine, "%s %i", buffString, &numVertices);
            break;
        }
    }
    
    graph->vcount = 0;
    
    vlist_init(&graph->vertices);
    
    //Now create the list of vertices in graph
    while (graph->vcount < numVertices) {
        //Insert a vertex for every one of numVertices
        vlist_insert_virtex( &(graph->vertices), graph->vcount);
        //Update the vcount
        graph->vcount = graph->vertices.size;
    }
    
    //Clear String
    for ( i=0; i<500; i++) {
        buffLine[i] = '\0';
    }
    
    //****************
    //SCANNING IN HUBS
    //****************
    
    //Now scan in one line of the file at a time until the string "Hubs:"
    while (fgets(buffLine, sizeof(buffLine), inputFile) != NULL) {
        
        //Scan the buffLine for the first string
        sscanf(buffLine, "%s", buffString);
        if (strcmp(buffString, "Hubs:") == 0) {
            
            //Now scan in all of the hub values
            for (i = 0; i<500; i++) {
                if (buffLine[i] <= '9' && buffLine[i] >= '0') {
                    hubID = atoi(&buffLine[i]);
                    
                    //Assign some random vertex pointer to the lookup of the hubID
                    vertex = vlist_lookup(&graph->vertices, hubID);
                    if (vertex != NULL) {
                        //If the vertex exists then set the hub flag to TRUE
                        vertex->hub = TRUE;
                        numHubs++;
                    }else{
                        //Else, something went wrong
                        printf("\nError: Nonexistant hub vertex defined\n");
                        exit(-1);
                    }
                }
            }
            break;   
        }
        
    }
    
    if (numHubs < 2) {
        printf("\nError: Not enough hubs\n");
        exit(-1);
    }
    
    //*****************
    //SCANNING IN EDGES
    //*****************
    
    //Scan in one line of the file at a time until the string "Edges:" is found
    while (fgets(buffLine, sizeof(buffLine), inputFile)!=NULL) {
        
        //This removes the possibility of adding a repeat vertex.
        if (buffLine[0] != '\n') {
            
            //Don't read in any edges until specified by the Edges line
            if (edgeDetect == TRUE) {
                
                sscanf(buffLine, "%d -> %d : %d", &startVertexID, &endVertexID, &edgeWeight);
                if( (vertex = vlist_lookup(&graph->vertices, startVertexID))==NULL){
                    printf("\nError: Incorrect source vertex\n");
                    exit(-1);
                }
                if( (adjVertex = vlist_lookup(&graph->vertices, endVertexID)) == NULL){
                    printf("\nError: Incorrect adjacent vertex\n");
                    exit(-1);
                }
                
                if (edgeWeight >= 0) {
                    if(elist_insert_edge(&vertex->edges, adjVertex, edgeWeight)==FALSE){
                        return FALSE;
                    }
                }else{
                    printf("Error: Negative Edge Weight Recieved in edge ( %d , %d) : %d\n", startVertexID, endVertexID, edgeWeight);
                    exit(-1);
                }

                
                for (i = 0; i<500; i++) {
                    buffLine[i]='\0';
                }
                
            }else{
                //Scan the buffLine for the first string
                sscanf(buffLine, "%s", buffString);
                
                if (strcmp(buffString, "Edges:") == 0){
                    edgeDetect = TRUE;
                }
            }
        }   
    }
    fclose(inputFile);
    return TRUE;
}

//This function will find the shortest path between hubs
bool analyze_graph( Graph *graph){
    
    //Some initalizations :D
    Vertex *startVertex;
    Vertex *currentStartHub;
    Vertex *currentFinishHub;
    Vertex *intermediateVertex;
    
    VertexList path;
    
    //First, all verticies in the graph will be scanned through. The first one to be a hub will be selected.
    for (startVertex=graph->vertices.head; startVertex != NULL; startVertex = startVertex->next) {
        
        //Now check to see if the vertex is a hub or not
        if (startVertex->hub == TRUE) {
            
            //This vertex is a hub. Use BellmanFord's Shortest path algorithm to find the shortest path to all other hubs
            //Note that in this program the distance through a hub to another hub cannot occur.
            //That is, the path from Hub 0 to Hub 2 cannot go through Hub 1
            
            if (bellmanFord(graph, startVertex) == FALSE) {
                printf("\nError: Something really, really bad happened...\n");
                printf("\nSeriously, you shouldn't be seeing this right now\n");
                exit(-1);
            }
            
            //This is for clarity of the code
            currentStartHub = startVertex;
            //*********************************************
            //GRAPH IS NOW SORTED WITH ORIGIN startVertex//
            //*********************************************
            //At this point the graph should be sorted for the Hub of some origin
            //What needs to be done now is to print out the distance from the final hub to the start hub
            for (currentFinishHub = graph->vertices.head; currentFinishHub != NULL; currentFinishHub = currentFinishHub->next) {
                
                if ((currentFinishHub->hub == TRUE)&&(currentFinishHub != startVertex)) {
                    
                    //*****************************//
                    //THIS PART IS THE EXTRA CREDIT//                    
                    //Okay, Now I'm going to create a VertexList that has the pred path
                    //I start from the Final hub and add each predecessor at the head of the list until the starting hub is reached.
                    //If there is no path available then the list will remain Null.
                    vlist_init(&path);
                    
                    //First insert the Final Hub
                    vlist_insert_virtex(&path, currentFinishHub->id);
                    for (intermediateVertex = currentFinishHub->pred; intermediateVertex != NULL; intermediateVertex = intermediateVertex->pred) {
                        
                        //Insert the predecessor vertex into the path if it exists
                        if ((intermediateVertex != NULL) && (intermediateVertex != startVertex)) {
                            vlist_insert_prev(&path, path.head, intermediateVertex->id);
                        }
                        
                    }
                    //Insert the Starting Hub at the head
                    vlist_insert_prev(&path, path.head, startVertex->id);
                    
                    //Now path should contain be a list of vertices starting from currentStartHub through the path and ending with currentFinalHub
                    //All that needs to be done is traversal and printing of this path
                    printf("<");
                    for (intermediateVertex = path.head; intermediateVertex != NULL; intermediateVertex = intermediateVertex->next) {
                        
                        printf(" %d ", intermediateVertex->id);
                        //Some pretty formatting
                        if (intermediateVertex->next != NULL) {
                            printf(",");
                        }
                    }
                    printf(" >");
                    
                    if (currentFinishHub->distance == -1) {
                        printf(" : Infinity\n");
                    }else{
                        printf(" : %d\n", currentFinishHub->distance);
                    }
                    
                    //Path is no longer necessary
                    //DESTROY IT!!!
                    vlist_destroy(&path);
                    
                    
                    //The following is for non-Extra Credit work
                    /*
                    printf("\n< %d ,..., %d >", currentStartHub->id, currentFinishHub->id );
                    if (currentFinishHub->distance == -1) {
                        printf(" : Infinity");
                    }else{
                        printf(" : %d", currentFinishHub->distance);
                    }
                    */
 
                }
            }
        }
    }
    
    //Some nice formatting
    printf("\n");

    return TRUE;
}

//This function will accept a graph and a start vertex and then compute the shortest distance to each hub
bool bellmanFord( Graph *graph, Vertex *startvertex){
    
    //Initalization of needed stuff
    Vertex *vertexU;
    Vertex *vertexV;
    Edge *edge;
    int i = 0;
    
    //Reset the all vertex distances to infinity
    reset_distances(&graph->vertices);
    
    //Initalize starting distance as zero
    startvertex->distance = 0;
    
    for (i = 0; i < graph->vertices.size; i++) {
        
        for(vertexU = graph->vertices.head; vertexU != NULL; vertexU = vertexU->next){
            
            for (edge = vertexU->edges.head; edge != NULL; edge=edge->next) {
                
                vertexV=edge->adjVertex;
                
                //This should stop the paths from going through a hub
                if (vertexU->hub == FALSE || vertexU == startvertex) {
                    bellmanFord_Relax(vertexU, edge);
                }
                
            }
        }
    }
    
    //I could leave this in, but I used -1 for infinity. Because of this, the following (which detects negative
    //cyclic graphs) was always being tripped.
//    for( vertexU = graph->vertices.head; vertexU != NULL; vertexU = vertexU->next){
//        for (edge = vertexU->edges.head; edge != NULL; edge=edge->next) {
//            vertexV = edge->adjVertex;
//            //Detect if a negative weight cycle exists
//            if (vertexV->distance > (vertexU->distance+edge->weight)) {
//                return FALSE;
//            }
//        }
//    }
    
    return TRUE;
}


//*********************************************************************************************************
//This function will reset the distances off all vertex elements in the list back to (-1) which is infinity
void reset_distances( VertexList *vlist){
    Vertex *vertex;
    
    for(vertex = vlist->head; vertex != NULL;vertex=vertex->next){
        vertex->distance = -1;
        vertex->pred = NULL;
    }
}

//********************************************************************************************************
//Relax is only used for BellmanFord search
//Reeeelaaxxxx
void bellmanFord_Relax(Vertex *u, Edge *e){
    
    Vertex *v;
    
    //For reference
    // u -edge-> v
    v = e->adjVertex;
    
    //-1 is being used for negative infinity
    if (((v->distance > (u->distance + e->weight)) || (v->distance == -1)) && (u->distance != -1)) {
        v->distance = u->distance + e->weight;
        v->pred = u;
    }
    
}









