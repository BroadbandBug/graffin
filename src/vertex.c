/**************************************************************************************************/
/*
 * File: vertex.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following is a declaration of the vertex structures and functions
 *              that manipulate them. vertex.h was created to be implemented with graffin
 *
 */
/**************************************************************************************************/

#include "vertex.h"

// Nothing to see here (yet), move along everyone (for now).

/**************************************************************************************************/
