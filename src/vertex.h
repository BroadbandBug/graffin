/**************************************************************************************************/
/*
 * File: vertex.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following is a declaration of the vertex structures and declarations of functions
 *              that manipulate them. vertex.h was created to be implemented with graffin
 *
 */
/**************************************************************************************************/

#ifndef VERTEX_H
#define VERTEX_H

/**************************************************************************************************/

#include "global.h"
#include "elist.h"

/**************************************************************************************************/

struct Vertex_ {
    int id;
    int distance;
    int color;
    bool hub;
    EdgeList edges;
    
    Vertex *pred;
    struct Vertex_ *next;
    struct Vertex_ *prev;
};

/**************************************************************************************************/

#endif

/**************************************************************************************************/

