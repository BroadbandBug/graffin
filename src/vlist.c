/**************************************************************************************************/
/*
 * File: vlist.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following are declarations of vertex list functions to be used in the
 *              implementation of graphs
 *
 */
/**************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "global.h"
#include "vertex.h"
#include "vlist.h"

/**************************************************************************************************/

/* initialize vlist to empty vlist */
void 
vlist_init(VertexList *vlist)
{
    if( vlist == NULL ) return;
    
    vlist->head = NULL;
    vlist->tail = NULL;
    vlist->size = 0;
}

/**************************************************************************************************/

/* destroy the vlist */
void 
vlist_destroy(VertexList *vlist)
{
    if( vlist == NULL ) return;
    
    /* while the list of list elements is not empty, remove the element at the head of the list */
    while( vlist->size > 0 ) {
        vlist_remove(vlist, vlist->head);
    }
}

/**************************************************************************************************/

/* inserts new vertex into vlist after the given vlist element, returns pointer to newly created
 * vertex is successful, NULL otherwise */
Vertex * 
vlist_insert_virtex(VertexList *vlist, int id)
{
    if( vlist == NULL ) return NULL;
    
    return vlist_insert_next(vlist, vlist->tail, id);
}

/**************************************************************************************************/

/* return pointer to vertex with specified id */
Vertex * 
vlist_lookup(VertexList *vlist, int id)
{
    if( vlist == NULL ) return NULL;

    Vertex *currentVertex;
    //Go through list of verticies and then 
    for (currentVertex = vlist->head; currentVertex != NULL; currentVertex=currentVertex->next) {
        if (currentVertex->id == id) {
            return currentVertex;
        }
    }
    
    return NULL;
}

/**************************************************************************************************/

/* removes the specified vertex from the vlist */
bool 
vlist_remove(VertexList *vlist, Vertex *vertex)
{
    if( vlist == NULL ) return FALSE;

    Vertex *currentVertex;
    Vertex *prevVertex;
    Vertex *nextVertex;
    
    currentVertex = vlist->head;
    
    //If this function is given NULL as the vertex pointer it will remove the head of the vlist
    if (vertex == NULL) {
        vertex = vlist->head;
    }
    while (currentVertex != NULL) {
        if (vertex == currentVertex) {
            
            //Case: Head of vertex list is being removed. Update the head of the list
            if (vertex == vlist->head) {
                vlist->head = vlist->head->next;
                if (vlist->size > 1) {

                    vlist->head->pred = NULL; 
                }
            }
            //Case: Tail of the vertex is being removed. Update the tail of the list to the tail's previous pointer
            else if(vertex == vlist->tail)
            {
                vlist->tail = vlist->tail->prev;
            }
            //Case: vertex is in the middle of the list
            //This requires the previous and next pointers of the other elements to be updated
            else{
                prevVertex = vertex->prev;
                nextVertex = vertex->next;
                
                prevVertex->next = nextVertex;
                nextVertex->prev = prevVertex;
            }
            //Now remove the element from the list
            free(currentVertex);
            vlist->size--;
            return TRUE;
        }
        currentVertex = currentVertex->next;
    }
    return FALSE;
}

/**************************************************************************************************/

/* inserts new vertex into vlist after the given vlist element */
Vertex * 
vlist_insert_next(VertexList *vlist, Vertex *vertex, int id)
{
    if( vlist == NULL ) return NULL;
    Vertex *currentVertex;
    Vertex *newVertex;

    //Create the new Vertex
    if ((newVertex = (Vertex *)malloc(sizeof(Vertex))) == NULL) {
        printf("\nError: Cannot allocate more memory\n");
        exit(-1);
    }
    
    //Assign Vertex id
    newVertex->id = id;
    //Assign Vertex Color as -1 for not discovered
    newVertex->color = -1;
    //Assign Vertex distance as -1 for infinite
    newVertex->distance = -1;
    //Assign Vertex as not a hub
    newVertex->hub = FALSE;
    
    //If the vlist size is zero then the head and tail pointers must be updated.
    if (vlist->size == 0) {
        vlist->head = newVertex;
        vlist->tail = newVertex;
        vlist->size++;
        return newVertex;
    }
    
    
    //Go through the entire list until the vertex is found
    for (currentVertex = vlist->head; currentVertex!=NULL; currentVertex=currentVertex->next) {
        
        //The current vertex has been found
        if (vertex == currentVertex) {
            
            //Case: The vertex pointer is the tail
            //Solution: Must update the vlist->tail pointer
            if (vertex == vlist->tail) {
                vertex->next = newVertex;
                newVertex->prev = vertex;
                newVertex->next = NULL;
                vlist->tail = newVertex;
            }
            //Case: the vertex pointer is located somewhere in the middle of the list
            //Solution: must update update currentVertex->previous & currentVertex->next pointers
            else{
                newVertex->next = vertex->next;
                vertex->next->prev = newVertex;
                vertex->next = newVertex;
                newVertex->prev = vertex;
  
            }
            
            vlist->size++;
            return newVertex;
        }
    }
    return NULL;
}

/**************************************************************************************************/

/* inserts new vertex into vlist before the given vlist element */
Vertex * 
vlist_insert_prev(VertexList *vlist, Vertex *vertex, int id)
{
    if( vlist == NULL ) return NULL;

    Vertex *currentVertex;
    Vertex *newVertex;
    
    //Create the new Vertex
    if ((newVertex = (Vertex *)malloc(sizeof(Vertex))) == NULL) {
        printf("\nError: Cannot allocate more memory\n");
        exit(-1);
    }
    //Assign Vertex id
    newVertex->id = id;
    //Assign Vertex Color as -1 for not discovered
    newVertex->color = -1;
    //Assign Vertex distance as -1 for infinite
    newVertex->distance = -1;
    //Assign Vertex as not a hub
    newVertex->hub = FALSE;
    
    //If the vlist has no elements then the head and tail pointers must be updated
    if (vlist->size == 0) {
        vlist->head = newVertex;
        vlist->tail = newVertex;
        vlist->size++;
        return newVertex;
    }
    
    //Go through the entire list until the vertex is found
    for (currentVertex = vlist->head; currentVertex!=NULL; currentVertex=currentVertex->next) {
        
        //The current vertex has been found
        if (vertex == currentVertex) {

            //Case: The vertex pointer is the head
            //Solution: Must update the vlist->head pointer
            if (vertex == vlist->head) {
                newVertex->next = vertex;
                vertex->prev = newVertex;
                vlist->head = newVertex;
                newVertex->prev = NULL;
                
            }
            //Case: the vertex pointer is located somewhere in the middle of the list
            //Solution: must update update currentVertex->previous & currentVertex->next pointers
            else{
                newVertex->next = vertex;
                newVertex->prev = vertex->prev;
                vertex->prev->next = newVertex;
                vertex->prev = newVertex;
                
            }
            
            vlist->size++;
            return newVertex;

        }
    }
    return NULL;
}

/**************************************************************************************************/

