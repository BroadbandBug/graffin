/**************************************************************************************************/
/*
 * File: graffin.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following are functions used in the implementation of graffin
 *
 */
/**************************************************************************************************/

#ifndef GRAFFIN_H
#define GRAFFIN_H

/**************************************************************************************************/

#include "global.h"
#include "vertex.h"
#include "graph.h"

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
/**************************************************************************************************/

//This function will read the inputfile and populate graph
bool populate_graph( Graph *graph, char *inputFileName);

//This function will find the shortest path between hubs
bool analyze_graph( Graph *graph);

//This function will reset the distances off all vertex elements in the list back to (-1) which is infinity
void reset_distances( VertexList *vlist);

//This function will accept a graph and a start vertex and then compute the shortest distance to each hub
bool bellmanFord( Graph *graph, Vertex *startvertex);

//Relax is only used for BellmanFord search
void bellmanFord_Relax(Vertex *u, Edge *e);

/**************************************************************************************************/

#endif

/**************************************************************************************************/

