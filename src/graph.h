/**************************************************************************************************/
/*
 * File: graph.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following are declarations of the Graph structure as well as various functions
 *              that perform manipulations on it.
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "vertex.h"
#include "edge.h"
#include "vlist.h"

/**************************************************************************************************/

#ifndef GRAPH_H
#define GRAPH_H

/**************************************************************************************************/

typedef struct Graph_ {
    VertexList vertices;
    int vcount;
    int ecount;
} Graph;

/**************************************************************************************************/

/* initialize graph to empty graph */
void graph_init(Graph *graph);

/* 
 * destroys the graph by freeing all memory allocated for each graph element returning the graph
 * to an empty graph
 */
void graph_destroy(Graph *graph);

/**************************************************************************************************/

#endif

/**************************************************************************************************/

