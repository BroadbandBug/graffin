/**************************************************************************************************/
/*
 * File: global.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following contains needed definitions for the implementationo of ECE275a's 
 *              fifth assignment 'graffin'
 *
 */
/**************************************************************************************************/

#ifndef GLOBAL_H
#define GLOBAL_H

/**************************************************************************************************/

/* Type declaration to define a Boolean variable type */

typedef int bool;
#define FALSE 0
#define TRUE 1

/**************************************************************************************************/

/* Type declarations for defining Vertex and Edge to resolve circular declarations */

typedef struct Vertex_ Vertex;
typedef struct Edge_ Edge;

/**************************************************************************************************/

#endif

/**************************************************************************************************/
