/**************************************************************************************************/
/*
 * File: global.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following contains needed functions for the implementation of ECE275a's 
 *              fifth assignment 'graffin'
 *
 */
/**************************************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include "graph.h"
#include "vlist.h"

/**************************************************************************************************/

/* initialize graph to empty graph */ 
void 
graph_init(Graph *graph)
{
    if( graph == NULL ) return;

    vlist_init(&(graph->vertices));
}

/**************************************************************************************************/

/* 
 * destroys the graph by destroying the list of vertices */
void 
graph_destroy(Graph *graph)
{
    if( graph == NULL ) return;

    vlist_destroy(&(graph->vertices));
}

/**************************************************************************************************/

