/**************************************************************************************************/
/*
 * File: elist.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following contains functions for the manipulation of an edge list
 *
 */
/**************************************************************************************************/

#ifndef ELIST_H
#define ELIST_H

/**************************************************************************************************/

#include "global.h"
#include "edge.h"
#include "vertex.h"

/**************************************************************************************************/

typedef struct EdgeList_ {
    Edge *head;
    Edge *tail;
    int size;
} EdgeList;

/**************************************************************************************************/

/* initialize elist to empty elist */
void elist_init(EdgeList *elist);

/* destroy the elist */
void elist_destroy(EdgeList *elist);

/* inserts new edge (Vertex *, weight) into elist */
bool elist_insert_edge(EdgeList *elist, Vertex *adjVirtex, int weight);

/* removes the specified edge from the vlist */
bool elist_remove(EdgeList *elist, Edge *edge);

/* inserts new vertex into vlist after the given vlist element */
bool elist_insert_next(EdgeList *elist, Edge *edge, Vertex *adjVirtex, int weight);

/* inserts new vertex into vlist before the given vlist element */
bool elist_insert_prev(EdgeList *elist, Edge *edge, Vertex *adjVirtex, int weight);

/**************************************************************************************************/

#endif

/**************************************************************************************************/

