/**************************************************************************************************/
/*
 * File: vlist.h
 * Author: Kevin Klug
 * Date Created: 3 December 2011
 *
 * Description: The following are declarations of vertex list functions to be used in the
 *              implementation of graphs
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "vertex.h"

/**************************************************************************************************/

#ifndef VLIST_H
#define VLIST_H

/**************************************************************************************************/

typedef struct VertexList_ {
    Vertex *head;
    Vertex *tail;
    int size;
} VertexList;

/**************************************************************************************************/

/* initialize vlist to empty vlist */
void vlist_init(VertexList *vlist);

/* destroys the vlist */
void vlist_destroy(VertexList *vlist);

/* inserts new vertex into vlist after the given vlist element, returns pointer to newly created
 * vertex is successful, NULL otherwise */
Vertex* vlist_insert_virtex(VertexList *vlist, int id);

/* inserts new vertex into vlist after the given vlist element */
Vertex * vlist_insert_next(VertexList *vlist, Vertex *location, int id);

/* inserts new vertex into vlist before the given vlist element */
Vertex * vlist_insert_prev(VertexList *vlist, Vertex *location, int id);

/* return pointer to vertex with specified id */
Vertex * vlist_lookup(VertexList *vlist, int id);

/* removes the specified vertex from the vlist */
bool vlist_remove(VertexList *vlist, Vertex *vertex);

/**************************************************************************************************/

#endif

/**************************************************************************************************/

