   ____     ____       _      _____   _____              _   _     
U /"___|uU |  _"\ uU  /"\  u |" ___| |" ___|    ___     | \ |"|    
\| |  _ / \| |_) |/ \/ _ \/ U| |_  uU| |_  u   |_"_|   <|  \| |>   
 | |_| |   |  _ <   / ___ \ \|  _|/ \|  _|/     | |    U| |\  |u   
  \____|   |_| \_\ /_/   \_\ |_|     |_|      U/| |\u   |_| \_|    
  _)(|_    //   \\_ \\    >> )(\\,-  )(\\,-.-,_|___|_,-.||   \\,-. 
 (__)__)  (__)  (__)__)  (__)__)(_/ (__)(_/ \_)-' '-(_/ (_")  (_/  
 
Graffin
 
Author: Kevin Klug
Date Created: 6 December 2011
 
Usage: graffin inputFile
 
Description:
 
	Graffin is a shortest path finder for graphs. Given a correctly formatted input file, graffin
	will find and enumerate the shortest path between all hubs. A path between two hubs cannot
 	include a third hub. Negative edge weights are not allowed for this version of graffin. However,
 	the BellmanFord path algorithm allows for it in general.
 	
Output will be formatted by increasing vertex id as follows:
< StartingVertexID, ... path ..., EndVertexID > : Total_Path_Weight
 	
______________
Example Graph:

        ( 0 ) --- 1 ---->( 1 )---- 7 ---->( 2 )
          |                /\               /\
          |                |                /
          |                |               /
          3                7              /
          |                |             /
          |                |            /
          \/               |           /
        ( 3 ) --- 2 ---->( 4 )--------1
		
Example inputFile for above graph:

Vertices: 5
Hubs: 0 1 2
Edges:
0 -> 1 : 1
0 -> 3 : 3
1 -> 2 : 7
3 -> 4 : 2
4 -> 2 : 1

Output for Above inputFile
< 0 , 1  > : 1
< 0 , 3 , 4 , 2  > : 6
< 1 , 0  > : Infinity
< 1 , 2  > : 7
< 2 , 0  > : Infinity
< 2 , 1  > : Infinity
